# list of bin/* to run after a successful build

xUNIT := sterna_devtools_testtools_aunit
xUNIT_LIB := --use=../../koskdevttestauni

xUNIT_GPL := aunit
xUNIT_GPL_LIB := 

BUILD_DIR := build

RUN_DIR := ./run
TST_DIR := ./run/tests
OUT_EXT := -out.txt
OUT_ERR := -err.txt
