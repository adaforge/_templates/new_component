separate (TestPerf)
   package body Timings is
      Timer : Timer_Time;
      procedure Advice (CrossPoint : JoinPoint) is
         use Real_Time;
         D : Duration;
      begin
         if True then -- Timings
            case CrossPoint is

               when Before_Set | Before_Get =>
                  Trace.Advice (CrossPoint); -- Trace
                  Timer.Start := Real_Time.Clock;

               when After_Set | After_Get =>
                  Timer.Stop := Real_Time.Clock;
                  Trace.Advice (CrossPoint); -- Trace

                  D := To_Duration (Timer.Stop - Timer.Start);

                  Text_IO.Put (Latin_1.HT & JoinPoint'Image (CrossPoint) & " Duration =");
                  Text_IO.Put (Duration'Image (D / Nb_of_calls));
                  Text_IO.Put_Line (" sec.");

            end case;
         end if;
      end Advice;
   end Timings;