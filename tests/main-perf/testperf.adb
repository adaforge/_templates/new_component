--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 20yy name_of_copyright_owner (name_of_copyright_owner@domain.ext)
--  SPDX-Creator: name_of_author (name_of_author@domain.ext)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 20yy-MM-DD
--  --------------------------------------------------------------------------------------

with Owner.Pkg1.Pkg2.MyComponent;
use Owner;

with Ada.Real_Time;

with Ada.Text_IO;
with Ada.Wide_Wide_Text_IO;
with Ada.Characters.Latin_1;

use Ada;
use Ada.Characters;

procedure TestPerf
is
   My_Name : constant Wide_Wide_String := "Ada";
   Nb_of_calls : constant Positive := 1_000_000;

   Recorded_Name : Wide_Wide_String (1 .. My_Name'Length);

   --  -------------------
   --  «Aspect» JoinPoints
   --  -------------------
      type JoinPoint is (Before_Set, After_Set, Before_Get, After_Get);

   --  ----------------
   --  Tracing «Aspect»
   --  ----------------
   package Trace is
      procedure Advice (CrossPoint : JoinPoint);
   end Trace;
   package body Trace is separate;
   --  use Trace;

   --  ----------------
   --  Timings «Aspect»
   --  ----------------
   package Timings is
      type Timer_Time is record
         Start, Stop : Real_Time.Time;
      end record;
      procedure Advice (CrossPoint : JoinPoint);
   end Timings;
   package body Timings is separate;
   --  use Timings;

begin
   --  --------------
   --      MAIN
   --  --------------

   Wide_Wide_Text_IO.Put_Line ("Hello ");

   Timings.Advice (Before_Set); -- Start Timer
   for I in 1 .. Nb_of_calls loop
      Pkg1.Pkg2.MyComponent.Recorded_Name.Set (My_Name);
   end loop;
   Timings.Advice (After_Set); -- Stop Timer

   Timings.Advice (Before_Get); -- Start Timer
   for I in 1 .. Nb_of_calls loop
      Recorded_Name := Pkg1.Pkg2.MyComponent.Recorded_Name.Get;
   end loop;
   Timings.Advice (After_Get); -- Stop Timer

   Wide_Wide_Text_IO.Put_Line (Recorded_Name);

end TestPerf;
