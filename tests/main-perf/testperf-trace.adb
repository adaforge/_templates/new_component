separate (TestPerf)
   package body Trace is
      procedure Advice (CrossPoint : JoinPoint) is
      begin
         if True then -- Trace
            case CrossPoint is

               when Before_Set | Before_Get =>
                  Text_IO.Put_Line (Latin_1.HT & "Start Timer ..." & JoinPoint'Image (CrossPoint));

               when After_Set | After_Get =>
                  Text_IO.Put_Line (Latin_1.HT & "Timer Stopped  " & JoinPoint'Image (CrossPoint));

            end case;
         end if;
      end Advice;
   end Trace;