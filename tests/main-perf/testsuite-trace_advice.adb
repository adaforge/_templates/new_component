separate (TestSuite)
   procedure Trace_Advice (CrossPoint : Trace_JoinPoint) is
   begin
      if Run_Args.Trace then -- Trace
         case CrossPoint is
            when Before_Get =>
               Wide_Wide_Text_IO.Put_Line ("   Calling lib function ...");
            when After_Get =>
               Wide_Wide_Text_IO.Put_Line ("   Back to main procedure...");
         end case;
      end if;
   end Trace_Advice;
