--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2022 STERNA MARINE s.a.s. (william.franck@sterna.io)
--  SPDX-Creator: William J. FRANCK (william.franck@sterna.io)

--  --------------------------------------------------------------------------------------
--  Initial creation date : 2020-01-10
--  --------------------------------------------------------------------------------------

with Owner.Pkg1.Pkg2.MyComponent;
use Owner;

with AUnit.Assertions; use AUnit.Assertions;

package body Test_MyLib_001 is

   --  ------------------------------
   package Register_Specific is
         new AUnit.Test_Cases.Specific_Test_Case_Registration (myTest_Case);
      use Register_Specific;

   --  ------------------------------
   --  Define the actual test routine
   --  ------------------------------
   procedure Get_Name_Init_01 (T : in out AUnit.Test_Cases.Test_Case'Class)  is separate;
   procedure Get_Name_Init_02 (T : in out AUnit.Test_Cases.Test_Case'Class)  is separate;
   procedure Get_Name_Init_03 (T : in out AUnit.Test_Cases.Test_Case'Class)  is separate;

   procedure Get_Name_EdgeCases_01 (T : in out AUnit.Test_Cases.Test_Case'Class)  is separate;
   procedure Get_Name (T : in out AUnit.Test_Cases.Test_Case'Class)  is separate;

   use Message_Strings;
   procedure Get_Name_Bis (T : in out myTest_Case'Class) is separate;

   procedure Set_Name_EdgeCases_01 (T : in out AUnit.Test_Cases.Test_Case'Class)  is separate;
   procedure Set_Name_EdgeCases_02 (T : in out AUnit.Test_Cases.Test_Case'Class)  is separate;

   procedure Set_Name (T : in out AUnit.Test_Cases.Test_Case'Class)  is separate;

   --  ------------------------------
   --  Identifier of test case
   --  ------------------------------
   overriding
   function Name (T : myTest_Case) return Message_String is
   begin
      return Format ("Test MyLib #001");
   end Name;

   --  ------------------------------
   --  Register test routines to call
   --  ------------------------------
   overriding
   procedure Register_Tests (T : in out myTest_Case) is
      use Test_Cases, Test_Cases.Registration;
   begin
      --  Repeat for each test routine

      Register_Routine (T, Get_Name_Init_01'Access, "#01-01: call Get() - EdgeCases - Init - BWWS");
      Register_Routine (T, Get_Name_Init_02'Access, "#01-02: call Get() - EdgeCases - Init - WWS");
      Register_Routine (T, Get_Name_Init_03'Access, "#01-03: call Get() - EdgeCases - Init - NS");

      Register_Routine (T, Set_Name_EdgeCases_01'Access, "#02-01: call Set() - EdgeCases - Null, Max_Name_Length");
      Register_Routine (T, Set_Name_EdgeCases_02'Access,
            "#02-02: call Set() - EdgeCases - NS'Last + 1 : exeption ADA.STRINGS.LENGTH_ERROR generated");

      Register_Routine (T, Set_Name'Access, "#03: call Set() - ASCII, Latin_1, Unicode Devanagari, Unicode Emoji");
      Register_Routine (T, Get_Name'Access, "#04: call Get() - ASCII, Latin_1, Unicode Devanagari, Unicode Emoji");
      Register_Routine (T, Get_Name_EdgeCases_01'Access, "#04: call Get() - EdgeCases - Null_String");

      Register_Wrapper (
           Test => T,
           Routine => Get_Name_Bis'Access,
           Name => "#00: call Get() - Ada with Emoji");

      --  Test #NN
      --  Register_...

   end Register_Tests;

   --  ------------------------------
   --  Data Setup
   --  ------------------------------
   overriding
   procedure Set_Up (T : in out myTest_Case) is separate;

   --  ------------------------------
   --  Data Tear Down
   --  ------------------------------
   overriding
   procedure Tear_Down (T : in out myTest_Case) is
   begin
      --  Do any necessary cleanups, so the next test
      --  has a clean environment.  If there is no
      --  cleanup, omit spec and body, as default is
      --  provided in Test_Cases.
      null;
   end Tear_Down;

end Test_MyLib_001;
