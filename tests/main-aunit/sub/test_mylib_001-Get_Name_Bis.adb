--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2022 STERNA MARINE s.a.s. (william.franck@sterna.io)
--  SPDX-Creator: William J. FRANCK (william.franck@sterna.io)

--  --------------------------------------------------------------------------------------
--  Initial creation date : 2020-01-10
--  --------------------------------------------------------------------------------------

separate (Test_MyLib_001)
   procedure Get_Name_Bis (T : in out myTest_Case'Class)
   is
   begin
      if Pkg1.Pkg2.MyComponent.Recorded_Name.Get /= T.Message_to_be_Sent
      then
         Assert (False, "Get() did NOT sent back the correct Unicode Emoji return value.");
      else
         Assert (True, "Correct return value was sent back to the caller.");
      end if;

   end Get_Name_Bis;
