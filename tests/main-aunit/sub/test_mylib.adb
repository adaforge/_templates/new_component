--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2022 STERNA MARINE s.a.s. (william.franck@sterna.io)
--  SPDX-Creator: William J. FRANCK (william.franck@sterna.io)

--  --------------------------------------------------------------------------------------
--  Initial creation date : 2020-01-10
--  --------------------------------------------------------------------------------------

--:FIXME change the name 'Test_MyLib_001'
with Test_MyLib_001;

with AUnit.Test_Cases; use AUnit.Test_Cases;

package body Test_MyLib is --:FIXME change the name 'Test_MyLib'

   function Suite return Access_Test_Suite
   is
      Test_001 : constant Test_Case_Access :=
            new Test_MyLib_001.myTest_Case; --:FIXME change the name 'Test_MyLib_001'
      Result   : constant Access_Test_Suite := new Test_Suite;
   begin
      Result.Add_Test (Test_001);
      return Result;
   end Suite;

end Test_MyLib;
