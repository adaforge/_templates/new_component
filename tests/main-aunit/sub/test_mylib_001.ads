--  with Owner.Pkg1.Pkg2.MyComponent;
--  use Owner;

with AUnit; use AUnit;
with AUnit.Test_Cases;

with Ada.Strings.Wide_Wide_Bounded;

package Test_MyLib_001 is

   package Message_Strings is new Ada.Strings.Wide_Wide_Bounded.Generic_Bounded_Length (512);

   --  Extend 'Test_Case' to hold Data to be transmitted to test case routine
   type myTest_Case is new AUnit.Test_Cases.Test_Case with record
      Message_to_be_Sent : Message_Strings.Bounded_Wide_Wide_String;
   end record;

   --  Name identifying the test case:
   overriding
   function Name (T : myTest_Case) return Message_String;

   --  Register routines to be run:
   overriding
   procedure Register_Tests (T : in out myTest_Case);

   --  Override if needed. Default empty implementations provided:

   --  Preparation performed before each routine:
   overriding
   procedure Set_Up (T : in out myTest_Case);

   --  Cleanup performed after each routine:
   overriding
   procedure Tear_Down (T :  in out myTest_Case);

end Test_MyLib_001;
