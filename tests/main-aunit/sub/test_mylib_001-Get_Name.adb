--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2022 STERNA MARINE s.a.s. (william.franck@sterna.io)
--  SPDX-Creator: William J. FRANCK (william.franck@sterna.io)

--  --------------------------------------------------------------------------------------
--  Initial creation date : 2020-01-10
--  --------------------------------------------------------------------------------------

separate (Test_MyLib_001)
   procedure Get_Name (T : in out AUnit.Test_Cases.Test_Case'Class)
   is
      pragma Unreferenced (T);
      Dummy_ASCII      : constant Wide_Wide_String := "Ada";
      Dummy_Latin_1    : constant Wide_Wide_String := "Jérôme";
      Dummy_Devanagari : constant Wide_Wide_String := "Ṣhiva";
      Dummy_Emoji      : constant Wide_Wide_String := "Ada 👩🏻‍💻";
   begin

      --  ASCII
      Pkg1.Pkg2.MyComponent.Recorded_Name.Set (Dummy_ASCII);
      ASCII :
      declare
         Dummy : constant Wide_Wide_String
               := Pkg1.Pkg2.MyComponent.Recorded_Name.Get;
      begin
         if Pkg1.Pkg2.MyComponent.Recorded_Name.Get /= Dummy_ASCII
         then
            Assert (False, "Get() did NOT sent back the correct ASCII return value.");
         end if;
      end ASCII;

      --  Latin_1
      Pkg1.Pkg2.MyComponent.Recorded_Name.Set (Dummy_Latin_1);
      Latin_1 :
      declare
         Dummy : constant Wide_Wide_String
               := Pkg1.Pkg2.MyComponent.Recorded_Name.Get;
      begin
         if Pkg1.Pkg2.MyComponent.Recorded_Name.Get /= Dummy_Latin_1
         then
            Assert (False, "Get() did NOT sent back the correct Latin_1 return value.");
         end if;
      end Latin_1;

      --  Unicode Devanagari
      Pkg1.Pkg2.MyComponent.Recorded_Name.Set (Dummy_Devanagari);
      Devanagari :
      declare
         Dummy : constant Wide_Wide_String
               := Pkg1.Pkg2.MyComponent.Recorded_Name.Get;
      begin
         if Pkg1.Pkg2.MyComponent.Recorded_Name.Get /= Dummy_Devanagari
         then
            Assert (False, "Get() did NOT sent back the correct Unicode Devanagari return value.");
         end if;
      end Devanagari;

      --  Unicode Emoji
      Pkg1.Pkg2.MyComponent.Recorded_Name.Set (Dummy_Emoji);
      Emoji :
      declare
         Dummy : constant Wide_Wide_String
               := Pkg1.Pkg2.MyComponent.Recorded_Name.Get;
      begin
         if Pkg1.Pkg2.MyComponent.Recorded_Name.Get /= Dummy_Emoji
         then
            Assert (False, "Get() did NOT sent back the correct Unicode Emoji return value.");
         end if;
      end Emoji;

      Assert (True, "Correct return value was sent back to the caller.");

   end Get_Name;
