--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2022 STERNA MARINE s.a.s. (william.franck@sterna.io)
--  SPDX-Creator: William J. FRANCK (william.franck@sterna.io)

--  --------------------------------------------------------------------------------------
--  Initial creation date : 2020-01-10
--  --------------------------------------------------------------------------------------

--:FIXME change the name 'MyLib'
with Test_MyLib;

package body Tests is

   function Suite return Access_Test_Suite
   is
      Result : constant Access_Test_Suite := AUnit.Test_Suites.New_Suite;
   begin
      Result.Add_Test (Test_MyLib.Suite); --:FIXME change the name 'MyLib'
      return Result;
   end Suite;

end Tests;
