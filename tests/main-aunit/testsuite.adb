--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2020 STERNA MARINE s.a.s. (william.franck@sterna.io)
--  SPDX-Creator: William J. FRANCK (william.franck@sterna.io)

--  --------------------------------------------------------------------------------------
--  Initial creation date : 2020-01-10
--  --------------------------------------------------------------------------------------
with AUnit.Reporter.Text;
use AUnit.Reporter.Text;

with AUnit.Run;
use AUnit.Run;

--  Suite for this level of tests:
with Tests;

procedure TestSuite is

   procedure Run is new Test_Runner (Tests.Suite);

   Reporter : Text_Reporter;

begin
   Set_Use_ANSI_Colors (Reporter, True);
   Run (Reporter);
end TestSuite;