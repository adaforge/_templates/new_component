--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 20yy name_of_copyright_owner (name_of_copyright_owner@domain.ext)
--  SPDX-Creator: name_of_author (name_of_author@domain.ext)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 20yy-MM-DD
--  --------------------------------------------------------------------------------------

with Mylib_TestSuite;

with Sterna.DevTools.TestTools.UnitTest;
with Sterna.DevTools.TestTools.UnitTest.TAP_Runner;
use Sterna.DevTools.TestTools;

procedure TAP_Runner is
   Suite : UnitTest.Test_Suite
         := Mylib_TestSuite.Get_Test_Suite;
begin
   UnitTest.TAP_Runner.Run (Suite);
end TAP_Runner;
