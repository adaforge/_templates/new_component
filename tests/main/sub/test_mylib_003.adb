--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2022 STERNA MARINE s.a.s. (william.franck@sterna.io)
--  SPDX-Creator: William J. FRANCK (william.franck@sterna.io)

--  --------------------------------------------------------------------------------------
--  Initial creation date : 2020-01-10
--  --------------------------------------------------------------------------------------

with Owner.Pkg1.Pkg2.MyComponent;
use Owner;

package body Test_MyLib_003 is

   --  ------------------------------
   --  Define the actual test routine
   --  ------------------------------
   procedure Set_Name_EdgeCases_01 (T : in out UnitTest.Test_Case'Class) is separate;
   procedure Set_Name_EdgeCases_02 (T : in out UnitTest.Test_Case'Class) is separate;

   procedure Set_Name (T : in out UnitTest.Test_Case'Class) is separate;

   --  ------------------------------
   --  Register test routines to call
   --  ------------------------------
   overriding
   procedure Initialize (T : in out myTest_Case) is
   begin
      --  Identifier of test case
      UnitTest.Set_Name (UnitTest.Test_Case (T), "MyComponent.Set()");

      --  Repeat for each test routine
      UnitTest.Add_Test_Routine (T, Set_Name_EdgeCases_01'Access,
            "#02-01: call Set() - EdgeCases - Null, Max_Name_Length");
      UnitTest.Add_Test_Routine (T, Set_Name_EdgeCases_02'Access,
            "#02-02: call Set() - EdgeCases - NS'Last + 1 : exeption ADA.STRINGS.LENGTH_ERROR generated");

      UnitTest.Add_Test_Routine (T, Set_Name'Access,
            "#03: call Set() - ASCII, Latin_1, Unicode Devanagari, Unicode Emoji");

   end Initialize;

   --  ------------------------------
   --  Data Setup
   --  ------------------------------
   overriding
   procedure Set_Up (T : in out myTest_Case) is
   begin
      null;
   end Set_Up;

   --  ------------------------------
   --  Data Tear Down
   --  ------------------------------
   overriding
   procedure Tear_Down (T : in out myTest_Case) is separate;

end Test_MyLib_003;
