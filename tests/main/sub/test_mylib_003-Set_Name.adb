--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2022 STERNA MARINE s.a.s. (william.franck@sterna.io)
--  SPDX-Creator: William J. FRANCK (william.franck@sterna.io)

--  --------------------------------------------------------------------------------------
--  Initial creation date : 2020-01-10
--  --------------------------------------------------------------------------------------

separate (Test_MyLib_003)
   procedure Set_Name (T : in out UnitTest.Test_Case'Class)
   is
      pragma Unreferenced (T);
   begin
      --  ASCII
      Pkg1.Pkg2.MyComponent.Recorded_Name.Set ("Ada");
      --  Latin_1
      Pkg1.Pkg2.MyComponent.Recorded_Name.Set ("Jérôme");
      --  Unicode Devanagari
      Pkg1.Pkg2.MyComponent.Recorded_Name.Set ("Ṣhiva");
      --  Unicode Emoji
      Pkg1.Pkg2.MyComponent.Recorded_Name.Set ("Ada 👩🏻‍💻");

      UnitTest.Assert (True, "Correct return value was sent back to the caller.");

   exception
      when others =>
         UnitTest.Assert (False, "Get() did NOT sent back the correct return value.");
         --  raise;

   end Set_Name;
