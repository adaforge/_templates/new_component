--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2022 STERNA MARINE s.a.s. (william.franck@sterna.io)
--  SPDX-Creator: William J. FRANCK (william.franck@sterna.io)

--  --------------------------------------------------------------------------------------
--  Initial creation date : 2020-01-10
--  --------------------------------------------------------------------------------------

separate (Test_MyLib_002)
   procedure Get_Name_EdgeCases_01 (T : in out UnitTest.Test_Case'Class)
   is
      pragma Unreferenced (T);
   begin
      --  Null_String
      Pkg1.Pkg2.MyComponent.Recorded_Name.Set ("");
      Null_String :
      declare
         Dummy : constant Wide_Wide_String
               := Pkg1.Pkg2.MyComponent.Recorded_Name.Get;
      begin
         UnitTest.Assert (False, "Get() did NOT generate exception NO_NAME_DEFINED on Null_String.");
      end Null_String;

   exception
      when Pkg1.Pkg2.MyComponent.NO_NAME_DEFINED =>
         UnitTest.Assert (True, "Expected exception on init case with Null_String value of Recorded_Name.");
      when others =>
         UnitTest.Assert (False, "Get() raised an unattended exception.");
   end Get_Name_EdgeCases_01;
