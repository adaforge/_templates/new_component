--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2022 STERNA MARINE s.a.s. (william.franck@sterna.io)
--  SPDX-Creator: William J. FRANCK (william.franck@sterna.io)

--  --------------------------------------------------------------------------------------
--  Initial creation date : 2020-01-10
--  --------------------------------------------------------------------------------------

separate (Test_MyLib_003)
   procedure Set_Name_EdgeCases_01 (T : in out UnitTest.Test_Case'Class)
   is
      pragma Unreferenced (T);
      Dummy : Wide_Wide_String (1 .. Pkg1.Pkg2.MyComponent.Max_Name_Length) := (others => 'x');
   begin
      Pkg1.Pkg2.MyComponent.Recorded_Name.Set ("");
      Pkg1.Pkg2.MyComponent.Recorded_Name.Set (Dummy);
      Pkg1.Pkg2.MyComponent.Recorded_Name.Set ('a' & "");

      UnitTest.Assert (True, "Correct call value was sent back to the caller.");

   end Set_Name_EdgeCases_01;
