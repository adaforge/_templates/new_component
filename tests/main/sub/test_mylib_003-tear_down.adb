--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2022 STERNA MARINE s.a.s. (william.franck@sterna.io)
--  SPDX-Creator: William J. FRANCK (william.franck@sterna.io)

--  --------------------------------------------------------------------------------------
--  Initial creation date : 2020-01-10
--  --------------------------------------------------------------------------------------

separate (Test_MyLib_003)
   overriding
   procedure Tear_Down (T : in out myTest_Case)
   is
   begin
      Pkg1.Pkg2.MyComponent.Recorded_Name.Set ("");
   end Tear_Down;
