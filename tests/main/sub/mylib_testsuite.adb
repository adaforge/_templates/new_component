
with Test_MyLib_002;
with Test_MyLib_003;

package body Mylib_TestSuite is
   --  use Sterna.DevTools.TestTools;

   function Get_Test_Suite return UnitTest.Test_Suite is

      TS : UnitTest.Test_Suite := UnitTest.Create_Suite ("MyLib");

      MyLib_002_Tests : Test_MyLib_002.myTest_Case;
      MyLib_003_Tests : Test_MyLib_003.myTest_Case;

   begin
      UnitTest.Add_Static_Test (TS, MyLib_002_Tests);
      UnitTest.Add_Static_Test (TS, MyLib_003_Tests);
      return TS;
   end Get_Test_Suite;

end Mylib_TestSuite;
