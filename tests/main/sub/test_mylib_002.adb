--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2022 STERNA MARINE s.a.s. (william.franck@sterna.io)
--  SPDX-Creator: William J. FRANCK (william.franck@sterna.io)

--  --------------------------------------------------------------------------------------
--  Initial creation date : 2020-01-10
--  --------------------------------------------------------------------------------------

with Owner.Pkg1.Pkg2.MyComponent;
use Owner;

package body Test_MyLib_002 is

   --  ------------------------------
   --  Define the actual test routine
   --  ------------------------------
   procedure Get_Name_Init_01 (T : in out UnitTest.Test_Case'Class) is separate;
   procedure Get_Name_Init_02 (T : in out UnitTest.Test_Case'Class) is separate;
   procedure Get_Name_Init_03 (T : in out UnitTest.Test_Case'Class) is separate;

   procedure Get_Name_EdgeCases_01 (T : in out UnitTest.Test_Case'Class) is separate;
   procedure Get_Name (T : in out UnitTest.Test_Case'Class) is separate;

   --  ------------------------------
   --  Register test routines to call
   --  ------------------------------
   overriding
   procedure Initialize (T : in out myTest_Case) is
--      use Test_Cases, Test_Cases.Registration;
   begin
      --  Identifier of test case
      UnitTest.Set_Name (UnitTest.Test_Case (T), "MyComponent.Get()");

      --  Repeat for each test routine
      UnitTest.Add_Test_Routine (T, Get_Name_Init_01'Access,
            "#01-01: call Get() - EdgeCases - Init - BWWS");
      UnitTest.Add_Test_Routine (T, Get_Name_Init_02'Access,
            "#01-02: call Get() - EdgeCases - Init - WWS");
      UnitTest.Add_Test_Routine (T, Get_Name_Init_03'Access,
            "#01-03: call Get() - EdgeCases - Init - NS");

      UnitTest.Add_Test_Routine (T, Get_Name'Access,
            "#04: call Get() - ASCII, Latin_1, Unicode Devanagari, Unicode Emoji");
      UnitTest.Add_Test_Routine (T, Get_Name_EdgeCases_01'Access,
            "#05: call Get() - EdgeCases - Null_String - raise exception NO_NAME_DEFINED");

   end Initialize;

   --  ------------------------------
   --  Data Setup
   --  ------------------------------
   overriding
   procedure Set_Up (T : in out myTest_Case) is separate;

   --  ------------------------------
   --  Data Tear Down
   --  ------------------------------
   overriding
   procedure Tear_Down (T : in out myTest_Case) is
   begin
      --  Do any necessary cleanups, so the next test
      --  has a clean environment.  If there is no
      --  cleanup, omit spec and body, as default is
      --  provided in Test_Cases.
      null;
   end Tear_Down;

end Test_MyLib_002;
