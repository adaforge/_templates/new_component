--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2022 STERNA MARINE s.a.s. (william.franck@sterna.io)
--  SPDX-Creator: William J. FRANCK (william.franck@sterna.io)

--  --------------------------------------------------------------------------------------
--  Initial creation date : 2020-01-10
--  --------------------------------------------------------------------------------------

separate (Test_MyLib_003)
   procedure Set_Name_EdgeCases_02 (T : in out UnitTest.Test_Case'Class)
   is
      pragma Unreferenced (T);
      Dummy : Wide_Wide_String (1 .. Pkg1.Pkg2.MyComponent.Max_Name_Length + 1)
         := (others => 'x');
   begin
      Pkg1.Pkg2.MyComponent.Recorded_Name.Set (Dummy);

   exception
      when Ada.Strings.Length_Error =>
         UnitTest.Assert (True, "Correct call value was sent back to the caller.");

   end Set_Name_EdgeCases_02;
