--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 20yy name_of_copyright_owner (name_of_copyright_owner@domain.ext)
--  SPDX-Creator: name_of_author (name_of_author@domain.ext)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 20yy-MM-DD
--  --------------------------------------------------------------------------------------

--  with Owner.Pkg1.Pkg2.MyComponent;
--  use Owner.Owner.Pkg1.Pkg2;

with Sterna.DevTools.TestTools.UnitTest;
use Sterna.DevTools.TestTools;

with Ada.Strings.Wide_Wide_Bounded;

package Test_MyLib_003 is

   package Message_Strings is new Ada.Strings.Wide_Wide_Bounded.Generic_Bounded_Length (512);

   --  Extend 'Test_Case' to hold Data to be transmitted to test case routine
   type myTest_Case is new UnitTest.Test_Case with record
      Message_to_be_Sent : Message_Strings.Bounded_Wide_Wide_String;
   end record;

   overriding
   procedure Initialize (T : in out myTest_Case);

   --  Preparation performed before each routine:
   overriding
   procedure Set_Up (T : in out myTest_Case);

   --  Cleanup performed after each routine:
   overriding
   procedure Tear_Down (T :  in out myTest_Case);

end Test_MyLib_003;
