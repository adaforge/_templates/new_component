--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2022 STERNA MARINE s.a.s. (william.franck@sterna.io)
--  SPDX-Creator: William J. FRANCK (william.franck@sterna.io)

--  --------------------------------------------------------------------------------------
--  Initial creation date : 2020-01-10
--  --------------------------------------------------------------------------------------

separate (Test_MyLib_002)
   overriding
   procedure Set_Up (T : in out myTest_Case)
   is
      Dummy_Emoji : Message_Strings.Bounded_Wide_Wide_String;
   begin
      Dummy_Emoji := Message_Strings.To_Bounded_Wide_Wide_String ("");
      T.Message_to_be_Sent := Dummy_Emoji;

      Pkg1.Pkg2.MyComponent.Recorded_Name.Set (
            Message_Strings.To_Wide_Wide_String (Dummy_Emoji));

   end Set_Up;
