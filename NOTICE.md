# Global Licences

## In the absence of a license in a source file

### Licence to be applied

[APACHE LICENSE, VERSION 2.0](<http://www.apache.org/licenses/LICENSE-2.0>
)

#### SPDX ID

SPDX-License-Identifier: Apache-2.0

SPDX-FileCopyrightText: Copyright 20yy name_of_copyright_owner (name_of_copyright_owner@domain.ext)

#### Text

Copyright [20yy] [name_of_copyright_owner]

Licensed under the Apache License, Version 2.0 (the "License"); 
you may not use this file except in compliance with the License. 
You may obtain a copy of the License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software 
distributed under the License is distributed on an "AS IS" BASIS, 
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
See the License for the specific language governing permissions and 
limitations under the License.

## In the of absence of a license about any artwork or documentation file

### Licence to be applied

Creative Commons Attribution Non Commercial Share Alike 2.0 Generic

#### SPDX ID

SPDX-License-Identifier: CC-BY-NC-SA-2.0

SPDX-FileCopyrightText: Copyright 20yy name_of_copyright_owner (name_of_copyright_owner@domain.ext)

#### Text

[Creative Commons Attribution-NonCommercial-ShareAlike 2.0](https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode)

<https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode>

CREATIVE COMMONS CORPORATION IS NOT A LAW FIRM AND DOES NOT PROVIDE LEGAL SERVICES. DISTRIBUTION OF THIS LICENSE DOES NOT CREATE AN ATTORNEY-CLIENT RELATIONSHIP. CREATIVE COMMONS PROVIDES THIS INFORMATION ON AN "AS-IS" BASIS. CREATIVE COMMONS MAKES NO WARRANTIES REGARDING THE INFORMATION PROVIDED, AND DISCLAIMS LIABILITY FOR DAMAGES RESULTING FROM ITS USE.

# END of Global Licence