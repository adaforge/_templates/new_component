--  Configuration for ownepkg1pkg2comp generated by Alire
pragma Restrictions (No_Elaboration_Code);
pragma Style_Checks (Off);

package Ownepkg1pkg2comp_Config is
   pragma Pure;

   Crate_Version : constant String := "0.1.0-dev";
   Crate_Name : constant String := "ownepkg1pkg2comp";

   Alire_Host_OS : constant String := "macos";

   Alire_Host_Arch : constant String := "x86_64";

   Alire_Host_Distro : constant String := "distro_unknown";

   type Build_Profile_Kind is (release, validation, development);
   Build_Profile : constant Build_Profile_Kind := validation;

   Buffer_Size_First : constant :=  0;
   Buffer_Size_Last : constant :=  1024;
   Buffer_Size : constant :=  256;

   type Trace_Level_Kind is (Info, Details, Debug);
   Trace_Level : constant Trace_Level_Kind := Info;

end Ownepkg1pkg2comp_Config;
