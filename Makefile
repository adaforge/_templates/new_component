SHELL = /bin/bash
.SUFFIXES:

# =============================================================
.PHONY: all rename rename_sources rename_files init build test run show

all: rename init build test
# =========================================================================
# Text and files  to search
# =========================

# Text
NAME_OF_COPYRIGHT_OWNER_tpl := "name_of_copyright_owner"
COPYRIGHT_EMAIL_tpl := "name_of_copyright_owner@domain.ext"
CREATION_DATE_tpl := "20yy-MM-DD"
UNIT_NAME     := "Owner.Pkg1.Pkg2.MyComponent"
FILE_NAME     := "owner-pkg1-pkg2-mycomponent"
BASE_NAME     := "OwnePkg1Pkg2Comp"

# Files to search
CONTENT_to_RENAME_DIRS := src bld
CONTENT_to_RENAME_FILES := *.gpr alire.toml
FILES_to_RENAME_DIRS := src bld
FILES_to_RENAME := *.gpr

include Makefile-renames.inc Makefile-run.inc

# =========================================================================
# Define static values
# ====================

# Tools used
GNATKR := gnatkr
TR := tr
SED := gsed
FIND := find 

# Intermediate resuts -- reconstructed for each run
RENAMES_DIR := renames
SED_FILE        = ${RENAMES_DIR}/RENAME_SOURCE.sed
SED_RENAME_FILE = ${RENAMES_DIR}/RENAME_FILE.sed

ifeq ($(origin OSTYPE), undefined)
RENAME_FILES := ${RENAMES_DIR}\\RENAME_FILES.bat
else
RENAME_FILES := ${RENAMES_DIR}/RENAME_FILES
endif

PROJECT_ROOT = ${PWD}

# HASH length
HASH_Length := 16
APP_Length := 16

# =========================================================================
UNIT_NAME_new := ${OWNER}.${PKG1}.${PKG2}.${MyCOMPONENT}

FILE_NAME_new := ${OWNER}-${PKG1}-${PKG2}-${MyCOMPONENT}
FILE_NAME_new := ${shell echo ${FILE_NAME_new} | ${TR} "[:upper:]" "[:lower:]"}

BASE_NAME_new := ${CRATE}

BASE_LNAME     := ${shell echo ${BASE_NAME} | ${TR} "[:upper:]" "[:lower:]"}
BASE_LNAME_new := ${shell echo ${BASE_NAME_new} | ${TR} "[:upper:]" "[:lower:]"}

BASE_FILE_NAME_new := ${BASE_LNAME_new}

LIB_NAME      := ${shell echo ${BASE_NAME} | ${TR} "[:lower:]" "[:upper:]"}
LIB_NAME_new  := ${shell echo ${BASE_NAME_new} | ${TR} "[:lower:]" "[:upper:]"}

# SHORT_NAME    := ${shell ${GNATKR} ${BASE_NAME} ${HASH_Length} | ${TR} "[:upper:]" "[:lower:]"}
# SHORT_NAME_new := ${shell ${GNATKR} ${BASE_NAME_new} ${HASH_Length} | ${TR} "[:upper:]" "[:lower:]"}
SHORT_NAME    := ${CRATE}
SHORT_NAME_new := ${CRATE}

APP_SHORT_NAME     := ${shell ${GNATKR} ${BASE_NAME}_myapp ${APP_Length} | ${TR} "[:upper:]" "[:lower:]"}
APP_SHORT_NAME_new := ${shell ${GNATKR} ${BASE_NAME_new}_myapp ${APP_Length} | ${TR} "[:upper:]" "[:lower:]"}

# =========================================================================
show:
	@echo "Unit : " $(UNIT_NAME) " ==> " $(UNIT_NAME_new)
	@echo "File : " $(FILE_NAME) " ==> " $(FILE_NAME_new)
	@echo "Base : " $(BASE_NAME) " ==> " $(BASE_NAME_new)
	@echo "Base : " $(BASE_LNAME) " ==> " $(BASE_LNAME_new)
	@echo "Base : " $(BASE_LNAME) " ==> " $(BASE_FILE_NAME_new)
	@echo "Lib  : " $(LIB_NAME) " ==> " $(LIB_NAME_new)
	@echo "Short: " $(SHORT_NAME) " ==> " $(SHORT_NAME_new)
	@echo "App  : " $(APP_SHORT_NAME) " ==> " $(APP_SHORT_NAME_new)
	
# ------------
run: | ${TST_DIR}
# ------------
	
	@cd ${RUN_DIR};for APP in ${PROJECT_ROOT}/bin/*; do \
	echo $$APP "======================================"; \
		$$APP > `basename $$APP`${OUT_EXT} 2> `basename $$APP`${OUT_ERR}; \
	done;
	@echo "===="

# ------------
test: | ${TST_DIR}
# ------------
	cd ${TST_DIR};for TEST in ${TESTSUITE}; do \
		echo $$TEST "======================================"; \
		$$TEST > `basename $$TEST`${OUT_EXT} 2> `basename $$TEST`${OUT_ERR}; \
	done;
	@echo "===="

${TST_DIR}:
	-find ${BUILD_DIR} -type d -name bin -exec ln -sfv "{}" . ";"
	mkdir -p ${TST_DIR}

# ------------
build:
# ------------
	@echo "build ========"
	gprbuild
#	alr build

# ------------
init:
# ------------
	@echo "init"
	-alr init --bin --in-place ${BASE_LNAME_new}
	-alr with ${xUNIT} ${xUNIT_LIB}
	-alr with ${xUNIT_GPL} ${xUNIT_GPL_LIB}

rename: rename_sources rename_files
# ----------
rename_files: | ${RENAMES_DIR}
# ----------
	@echo "rename files ========="
	@echo "s/\(.*\)/mv -v \1 \1/;s/[oO]wner/\\L${OWNER}/2;s/[pP]kg1/\\L${PKG1}/2;s/[pP]kg2/\\L${PKG2}/2;s/[mM]y[cC]omponent/\\L${MyCOMPONENT}/2" > ${SED_RENAME_FILE}
	
# ==> Find the files to rename
	-rm  ${RENAME_FILES}
	for DD in ${FILES_to_RENAME_DIRS}; do \
		find $$DD -type f -iname "owner*.*" >> ${RENAME_FILES}; \
		done;
	for FF in ${FILES_to_RENAME}; do \
		find . -iname $$FF >> ${RENAME_FILES};\
		done;
# ==> Rename all the files
	${SED} -f ${SED_RENAME_FILE} ${RENAME_FILES} > ${RENAME_FILES}.sh 
	chmod +x ${RENAME_FILES}.sh
	./${RENAME_FILES}.sh

# ------------
rename_sources: | ${RENAMES_DIR}
# ------------
	@echo "rename ======="
	@echo "s/"$(NAME_OF_COPYRIGHT_OWNER_tpl)"/"$(NAME_OF_COPYRIGHT_OWNER)"/" > ${SED_FILE}
	@echo "s/"$(COPYRIGHT_EMAIL_tpl)"/"$(COPYRIGHT_EMAIL)"/" >> ${SED_FILE}
	@echo "s/"$(CREATION_DATE_tpl)"/"$(CREATION_DATE)"/" >> ${SED_FILE}
	@echo "s/"$(subst .,\\.,$(UNIT_NAME))"/"$(subst .,\\.,$(UNIT_NAME_new))"/g" >> ${SED_FILE}
	@echo "s/"$(FILE_NAME)"/"$(FILE_NAME_new)"/g" >> ${SED_FILE}
	@echo "s/"$(BASE_NAME)"/"$(BASE_NAME_new)"/g" >> ${SED_FILE}
	@echo "s/"$(BASE_LNAME)"/"$(BASE_LNAME_new)"/g" >> ${SED_FILE}
	@echo "s/"$(LIB_NAME)"/"$(LIB_NAME_new)"/g" >> ${SED_FILE}
	@echo "s/"$(SHORT_NAME)"/"$(SHORT_NAME_new)"/g" >> ${SED_FILE}
	@echo "s/"$(APP_SHORT_NAME)"/"$(APP_SHORT_NAME_new)"/g" >> ${SED_FILE}
	@echo "s/[oO]wner/"${OWNER}"/g" >> ${SED_FILE}
	@echo "s/[pP]kg1/"${PKG1}"/g" >> ${SED_FILE}
	@echo "s/[pP]kg2/"${PKG2}"/g" >> ${SED_FILE}
	@echo "s/[mM]y[cC]omponent/"${MyCOMPONENT}"/g" >> ${SED_FILE}
# ==> Modify sources files
	for DD in ${CONTENT_to_RENAME_DIRS}; do \
		find $$DD -type f -exec ${SED} -i -f ${SED_FILE} "{}" ";";\
		done;
	for FF in ${CONTENT_to_RENAME_FILES}; do \
		${SED} -i -f ${SED_FILE} $$FF;\
		done;

${RENAMES_DIR}:
	-mkdir -p ${RENAMES_DIR}

