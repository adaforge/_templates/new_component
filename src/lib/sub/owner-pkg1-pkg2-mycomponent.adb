--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 20yy name_of_copyright_owner (name_of_copyright_owner@domain.ext)
--  SPDX-Creator: name_of_author (name_of_author@domain.ext)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 20yy-MM-DD
--  --------------------------------------------------------------------------------------

package body Owner.Pkg1.Pkg2.MyComponent is

   protected body Recorded_Name is

      -- ---------
      --  Set
      -- ---------
      procedure Set  (Name : Wide_Wide_String)
      is
      begin
         Some_Name := Strings_for_Names.To_Bounded_Wide_Wide_String (Name);
      end Set;

      -- ---------
      --  Get
      -- ---------
      function Get return Wide_Wide_String
      is
         use Strings_for_Names;
      begin
         if Length (Some_Name) /= 0 then
            return Strings_for_Names.To_Wide_Wide_String (Some_Name);
         else
            raise NO_NAME_DEFINED;
         end if;
      end Get;

      function Get return String_Name
      is
      begin
         if Strings_for_Names.Length (Some_Name) /= 0 then
            return Some_Name;
         else
            raise NO_NAME_DEFINED;
         end if;
      end Get;

   end Recorded_Name;

end Owner.Pkg1.Pkg2.MyComponent;
