pragma Wide_Character_Encoding (UTF8);
--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 20yy name_of_copyright_owner (name_of_copyright_owner@domain.ext)
--  SPDX-Creator: name_of_author (name_of_author@domain.ext)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 20yy-MM-DD
--  --------------------------------------------------------------------------------------
--
--  @summary
--
--  @description
--
--  --------------------------------------------------------------------------------------

with Ada.Strings.Wide_Wide_Bounded;
use Ada;

package Owner.Pkg1.Pkg2.MyComponent is

   Max_Name_Length : constant Positive := 48;

   Name_of_Ada : constant Wide_Wide_String := "Ada 👩🏻‍💻";
   --  Visible for the Unit Testing Suite

   NO_NAME_DEFINED : exception;

      package Strings_for_Names is new Strings.Wide_Wide_Bounded.Generic_Bounded_Length (Max_Name_Length);
      subtype String_Name is Strings_for_Names.Bounded_Wide_Wide_String;

   protected Recorded_Name is

      procedure Set  (Name : Wide_Wide_String);

      function Get return Wide_Wide_String;
      function Get return String_Name;

   private
      Some_Name : String_Name := Strings_for_Names.Null_Bounded_Wide_Wide_String;
   end Recorded_Name;

end Owner.Pkg1.Pkg2.MyComponent;
