--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 20yy name_of_copyright_owner (name_of_copyright_owner@domain.ext)
--  SPDX-Creator: name_of_author (name_of_author@domain.ext)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 20yy-MM-DD
--  --------------------------------------------------------------------------------------
--
--  @summary
--
--  @description
--
--  --------------------------------------------------------------------------------------

with Owner.Pkg1.Pkg2.MyComponent;
use Owner;

with Command_Line;
use Command_Line;

with Ada.Wide_Wide_Text_IO;
use Ada;

procedure Owner_Pkg1_Pkg2_MyComponent
is

   --  --------------
   --  Trace «Aspect»
   --  --------------
   package Trace is
      type JoinPoint is (Before_Get, After_Get, Before_Set, After_Set);
      procedure Advice (CrossPoint : JoinPoint);
   end Trace;
   package body Trace is separate;
   use Trace;

begin
   --  --------------
   --      MAIN
   --  --------------
   --  get the command line arguments
   Command_Line.Get_Args (Args => Run_Args);

   Wide_Wide_Text_IO.Put ("Hello ");

   Trace.Advice (Before_Set); -- Trace
   Pkg1.Pkg2.MyComponent.Recorded_Name.Set ("Ada");
   Trace.Advice (After_Set); -- Trace

   Trace.Advice (Before_Get); -- Trace
   Wide_Wide_Text_IO.Put (Pkg1.Pkg2.MyComponent.Recorded_Name.Get);
   Wide_Wide_Text_IO.New_Line;
   Trace.Advice (After_Get); -- Trace

end Owner_Pkg1_Pkg2_MyComponent;
