--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2020 STERNA MARINE s.a.s. (william.franck@sterna.io)
--  SPDX-Creator: William J. FRANCK (william.franck@sterna.io)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 2020-01-10
--  --------------------------------------------------------------------------------------

with Ada.Command_Line;

with Ada.Characters.Latin_1;
with Ada.Characters.Handling;

with Ada.Text_IO;

use Ada;
use Ada.Characters;

package body Command_Line is

   --  --------
   --  Get_Args
   --  --------
   procedure Get_Args (Args : in out Program_args) is

      package ACL renames Ada.Command_Line;

   begin

      --  Help ?
      for i in 1 .. ACL.Argument_Count loop
         if ACL.Argument (i) = "-h"
         or else ACL.Argument (i) = "--help"
         then
            raise BAD_ARGUMENTS;
         end if;
      end loop;

      case ACL.Argument_Count is
         when 0 =>
            Args.Data_File_Name := Null_Bounded_String;

         when 1 =>
            Args.Data_File_Name := To_Bounded_String (ACL.Argument (1));

         when 2 =>

            if ACL.Argument (1) = "-t"
            or else ACL.Argument (1) =  "--" & Handling.To_Lower (Defined_Arg'Image (TRACE))
            then
               Args.Trace := True;

            elsif ACL.Argument (1) = "-T"
            or else ACL.Argument (1) = "--" & Handling.To_Lower (Defined_Arg'Image (TIMINGS))
            then
               Args.Timings := True;

            else
               raise BAD_ARGUMENTS;
            end if;

            Args.Data_File_Name := To_Bounded_String (ACL.Argument (2));

         when others =>
            raise BAD_ARGUMENTS;
      end case;

   exception
      when BAD_ARGUMENTS =>
         Text_IO.New_Line (Text_IO.Standard_Error);
         Text_IO.Put_Line
           (Text_IO.Standard_Error,
            "Usage : " & ACL.Command_Name & " [ -t | -T ] file_name ");
         Text_IO.Put_Line (Text_IO.Standard_Error, "Options:");
         Text_IO.Put_Line
           (Text_IO.Standard_Error, Latin_1.HT & "-t --trace   : Trace of intermediate results");
         Text_IO.Put_Line
           (Text_IO.Standard_Error, Latin_1.HT & "-T --timings : Trace of intermediate results");
         Text_IO.New_Line (Text_IO.Standard_Error);
         ACL.Set_Exit_Status (ACL.Failure);
         raise;
   end Get_Args;

end Command_Line;
