separate (Owner_Pkg1_Pkg2_MyComponent)
package body Trace is
   procedure Advice (CrossPoint : JoinPoint) is
   begin
      if Run_Args.Trace then -- Trace
         case CrossPoint is

            when Before_Get =>
               Wide_Wide_Text_IO.Put_Line ("   Calling lib function 'Get' ...");
            when After_Get =>
               Wide_Wide_Text_IO.Put_Line ("   Back to main procedure.");

            when Before_Set =>
               Wide_Wide_Text_IO.Put_Line ("   Calling lib procedure 'Set' ...");
            when After_Set =>
               Wide_Wide_Text_IO.Put_Line ("   Back to main procedure.");

         end case;
      end if;
   end Advice;
end Trace;