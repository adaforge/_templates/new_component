--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2020 STERNA MARINE s.a.s. (william.franck@sterna.io)
--  SPDX-Creator: William J. FRANCK (william.franck@sterna.io)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 2020-01-10
--  --------------------------------------------------------------------------------------
--
--  @summary
--
--  @description
--
--  --------------------------------------------------------------------------------------

with Ada.Strings.Bounded;

package Command_Line is

   package OS_File_Name is new Ada.Strings.Bounded.Generic_Bounded_Length
     (1_024);
   use OS_File_Name;

   type Defined_Arg is (TRACE, TIMINGS);

   type Program_args is record
      Trace          : Boolean := False;
      Timings        : Boolean := False;
      Data_File_Name : Bounded_String := Null_Bounded_String;
   end record;

   BAD_ARGUMENTS : exception;

   procedure Get_Args (Args : in out Program_args);

   Run_Args : Command_Line.Program_args;

end Command_Line;
