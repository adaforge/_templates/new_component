--  If the program does terminal interaction,
--  make it output a short notice like this when it starts in an interactive mode: 
   Text_IO.Put_Line (Standard_Error, "    <program>  Copyright (C) 2023 name_of_copyright_owner");
   Text_IO.Put_Line (Standard_Error, "This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.");
   Text_IO.Put_Line (Standard_Error, "This is free software, and you are welcome to redistribute it");
   Text_IO.Put_Line (Standard_Error, "under certain conditions; type `show c' for details.");

--   show w
   Text_IO.Put_Line (Standard_Error, "THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY");
   Text_IO.Put_Line (Standard_Error, "APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT");
   Text_IO.Put_Line (Standard_Error, "HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY");
   Text_IO.Put_Line (Standard_Error, "OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,");
   Text_IO.Put_Line (Standard_Error, "THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR");
   Text_IO.Put_Line (Standard_Error, "PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM");
   Text_IO.Put_Line (Standard_Error, "IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF");
   Text_IO.Put_Line (Standard_Error, "ALL NECESSARY SERVICING, REPAIR OR CORRECTION.");

--   show c
