--  If the program does terminal interaction,
--  make it output a short notice like this when it starts in an interactive mode: 
   Text_IO.Put_Line (Standard_Error, "    <program>  Copyright (C) 2023  name_of_copyright_owner");
   Text_IO.Put_Line (Standard_Error, "This software is provided by **<name of author> and contributors** ""as is"".");

--   show w
   Text_IO.Put_Line (Standard_Error, "Any express or implied warranties, including, but not limited to, the implied");
   Text_IO.Put_Line (Standard_Error, "warranties of merchantability and fitness for a particular purpose are");
   Text_IO.Put_Line (Standard_Error, "disclaimed. in no event shall **name_of_copyright_owner or contributors** be liable for any");
   Text_IO.Put_Line (Standard_Error, "direct, indirect, incidental, special, exemplary, or consequential damages");
   Text_IO.Put_Line (Standard_Error, "(including, but not limited to, procurement of substitute goods or services;");
   Text_IO.Put_Line (Standard_Error, "loss of use, data, or profits; or business interruption) however caused and");
   Text_IO.Put_Line (Standard_Error, "on any theory of liability, whether in contract, strict liability, or tort");
   Text_IO.Put_Line (Standard_Error, "(including negligence or otherwise) arising in any way out of the use of this");
   Text_IO.Put_Line (Standard_Error, "software, even if advised of the possibility of such damage.");
--   show c
