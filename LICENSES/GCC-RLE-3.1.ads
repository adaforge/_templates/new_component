--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: GPL-3.0-with-GCC-exception
--  SPDX-FileCopyrightText: Copyright 2023 name_of_copyright_owner (name_of_copyright_owner@domain.ext)
--  SPDX-Creator: name_of_copyright_owner (name_of_copyright_owner@domain.ext)
--  --------------------------------------------------------------------------------------
--  This library is free software;  you can redistribute it and/or modify it
--  under terms of the  GNU General Public License  as published by the Free
--  Software  Foundation;  either version 3,  or (at your  option) any later
--  version. This library is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN-
--  TABILITY or FITNESS FOR A PARTICULAR PURPOSE.
--
--  As a special exception under Section 7 of GPL version 3, you are granted
--  additional permissions described in the GCC Runtime Library Exception,
--  version 3.1, as published by the Free Software Foundation.
--
--  You should have received a copy of the GNU General Public License and
--  a copy of the GCC Runtime Library Exception along with this program;
--  see the files 'GPL-3.0-only.md' and 'GCC-RLE-3.1.md' respectively.
--  If not, see <https://www.gnu.org/licenses/gpl-3.0-standalone.html>
--  <https://www.gnu.org/licenses/gcc-exception-3.1.html>.
--  --------------------------------------------------------------------------------------
--  Initial creation date : 20yy-MM-DD
--  --------------------------------------------------------------------------------------
--
--  @summary
--
--  @description
--
--  --------------------------------------------------------------------------------------
pragma License (Modified_GPL);
