# GNAT Modified General Public License

> Version 1.0 \
> SPDX-License-Identifier: `GMGPL-1.0` \
> <https://www.copyleftlicense.com/licenses/gnat-modified-general-public-license-(gmgpl)-version-10/> \
> Copyright (C) 1999-2009, AdaCore

## GNAT RUN-TIME COMPONENTS

G N A T . T R A C E B A C K . S Y M B O L I C

GNAT is free software; you can redistribute it and/or modify it under terms of the **GNU General Public License** as published by the Free Software Foundation; **either version 2, or (at your option) any later version**.

GNAT is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License distributed with GNAT; see file COPYING. If not, write to the Free Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

**As a special exception**, if other files instantiate generics from this unit, or you link this unit with other files to produce an executable, this unit does NOT by itself cause the resulting executable to be covered by the GNU General Public License.
This exception does not however invalidate any other reasons why the executable file might be covered by the GNU Public License.

GNAT was originally developed by the GNAT team at New York University.
Extensive contributions were provided by Ada Core Technologies Inc
