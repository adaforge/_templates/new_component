# DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE

> Version 2, December 2004 \
> SPDX-License-Identifier: `WTFPL` \
> <http://www.wtfpl.net/about/> \
> by Sam Hocevar

![WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/logo-220x1601.png)

## Contract

Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed **as long
 as the name is changed**.

### TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

0. You just DO WHAT THE FUCK YOU WANT TO.

## End of Contract
