--  If the program does terminal interaction,
--  make it output a short notice like this when it starts in an interactive mode: 
   Text_IO.Put_Line (Standard_Error, "    <program>  Copyright (C) 2023  name_of_copyright_owner");
   Text_IO.Put_Line (Standard_Error, "This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.");
   Text_IO.Put_Line (Standard_Error, "This is free software, and you are welcome to redistribute it");
   Text_IO.Put_Line (Standard_Error, "under certain conditions; type `show c' for details.");

--   show w
   Text_IO.Put_Line (Standard_Error, "Unless required by applicable law or agreed to in writing,");
   Text_IO.Put_Line (Standard_Error, "Licensor provides the Work (and each Contributor provides its Contributions)");
   Text_IO.Put_Line (Standard_Error, "on an ""*AS IS*"" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,");
   Text_IO.Put_Line (Standard_Error, "either express or implied, including, without limitation, any warranties");
   Text_IO.Put_Line (Standard_Error, "or conditions of TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS");
   Text_IO.Put_Line (Standard_Error, "FOR A PARTICULAR PURPOSE.");
   Text_IO.Put_Line (Standard_Error, "You are solely responsible for determining the appropriateness of using");
   Text_IO.Put_Line (Standard_Error, "or redistributing the Work and assume any risks associated with ");
   Text_IO.Put_Line (Standard_Error, "Your exercise of permissions under this License.");
--   show c
