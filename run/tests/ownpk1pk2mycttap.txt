TAP version 14
1..8
ok 1 - MyLib.Get(): #01-01: call Get() - EdgeCases - Init - BWWS
ok 2 - MyLib.Get(): #01-02: call Get() - EdgeCases - Init - WWS
ok 3 - MyLib.Get(): #01-03: call Get() - EdgeCases - Init - NS
ok 4 - MyLib.Get(): #04: call Get() - ASCII, Latin_1, Unicode Devanagari, Unicode Emoji
ok 5 - MyLib.Get(): #05: call Get() - EdgeCases - Null_String - raise exception NO_NAME_DEFINED
ok 6 - MyLib.Set(): #02-01: call Set() - EdgeCases - Null, Max_Name_Length
ok 7 - MyLib.Set(): #02-02: call Set() - EdgeCases - NS'Last + 1 : exeption ADA.STRINGS.LENGTH_ERROR generated
ok 8 - MyLib.Set(): #03: call Set() - ASCII, Latin_1, Unicode Devanagari, Unicode Emoji

