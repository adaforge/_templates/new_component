# Renames of files, Unit-names, code
OWNER := Owner
PKG1 := Pkg1
PKG2 := Pkg2
MyCOMPONENT := MyComponent
CRATE := ownepkg1pkg2comp 
NAME_OF_COPYRIGHT_OWNER := "name_of_copyright_owner"
COPYRIGHT_EMAIL := "name_of_copyright_owner"
CREATION_DATE := "20yy-MM-DD"

MyLIB := MyLib
MyAPP := MyApp

MAIN := ownepkg1pkg2comp
TESTSUITE = ${PROJECT_ROOT}/bin/ownpk1pk2myctest ${PROJECT_ROOT}/bin/ownpk1pk2mycperf
