# Purpose

Template of files to initialize a component.

* **Library API** (public `*.ads`) are in `src/lib`
* **Main** program(s) are in `src/main`
* **Unit Test program** and **Performance** are in `src/test`

Sources can be build with Alire `alr` or AdaCore `gprbuild`.

## Tools used

1. The build is conducted by [gprbuild](https://docs.adacore.com/live/wave/gprbuild/html/gprbuild_ug/gprbuild_ug.html).

2. The dependencies on external components are managed by [alr](https://alire.ada.dev/docs/) tool.

3. Unit testing framework is [AUnit](https://docs.adacore.com/live/wave/aunit/html/aunit_cb/aunit_cb.html).
Thought AUnit it seems a bit tough, it is of great value for debugging and hardening your code. See [TDD](https://en.wikipedia.org/wiki/Test-driven_development) !

4. You can find some Ada Source components through [AdaForge's Repository](https://www.adaforge.org/Libraries/).

## Usage

### Phase 1

Clone the template and build-run **AI-IS**.

```shell
$ git clone git@gitlab.com:adaforge/_templates/new_component.git mycomponent
$ cd mycomponent
$ alr init --bin --in-place owner_pkg1_pkg2_mycomponent
$ alr with aunit
$ alr build

ⓘ Building owner_pkg1_pkg2_mycomponent/owner_pkg1_pkg2_mycomponent.gpr...Compile
   [Ada]          owner_pkg1_pkg2_mycomponent_myapp.adb
   [Ada]          testsuite.adb
   [Ada]          testperf.adb
   [Ada]          owner_pkg1_pkg2_mycomponent_config.ads
   [Ada]          owner.ads
   [Ada]          owner-pkg1-pkg2-mycomponent-mylib.adb
   [Ada]          owner-pkg1-pkg2-mycomponent.ads
   [Ada]          owner-pkg1.ads
   [Ada]          owner-pkg1-pkg2.ads
   [Ada]          command_line.adb
   [Ada]          tests.adb
   [Ada]          test_mylib.adb
   [Ada]          test_mylib_001.adb
Build Libraries
   [gprlib]       Owner_Pkg1_Pkg2_MyComponent.lexch
   [archive]      libOwner_Pkg1_Pkg2_MyComponent.a
   [index]        libOwner_Pkg1_Pkg2_MyComponent.a
Bind
   [gprbind]      owner_pkg1_pkg2_mycomponent_myapp.bexch
   [Ada]          owner_pkg1_pkg2_mycomponent_myapp.ali
   [gprbind]      testsuite.bexch
   [Ada]          testsuite.ali
   [gprbind]      testperf.bexch
   [Ada]          testperf.ali
Link
   [link]         owner_pkg1_pkg2_mycomponent_myapp.adb
   [link]         testsuite.adb
   [link]         testperf.adb
Build finished successfully in 1.61 seconds.
```

```shell
$ ln -sv build/development/x86_64-apple-darwin22.1.0/bin bin
```

### Check the results

_Note_ : `FAIL Test MyLib #001 : #02: call Get()` is intended to show an inexpected result.

#### Run the unit tests

```shell
$ bin/ownpk1pk2myctest 

OK Test MyLib #001 : #01: call Get()

FAIL Test MyLib #001 : #02: call Get()
    Get() did NOT sent back the correct return value.
    at test_mylib_001-unit_test_02.adb:21

Total Tests Run:   2
Successful Tests:  1
Failed Assertions: 1
Unexpected Errors: 0
```

#### Run the app

```shell
$ bin/ownpk1pk2mycmyap 
Hello Ada 👩🏻‍💻
```

#### Run the performance test

```shell
$ bin/ownpk1pk2mycperf

Hello 
	Start Timer ...
	Timer Stopped.
	Duration = 0.000000011 sec.
Ada 👩🏻‍💻
```

### Phase 2

* Owner --> ...
* Pkg1 --> ...
* Pkg2 --> ...
* MyComponent --> ...
* MyLib --> ...
* MyApp --> ...

==> Modify the names of files accordingly to your needs. [`*.ads *.adb *.gpr`]

==> Modify the Unit Names accordingly to your needs. [`*.ads *.adb *.gpr*`]

==> In any/every [`*.ads *.adb alire.toml`] file heading
  * 20yy name_of_copyright_owner name_of_copyright_owner@domain.ext
  * name_of_author (name_of_author@domain.ext)
  * Initial creation date : 20yy-MM-DD

_Licenses_: Please feel free to change the licenses blabla...

* LICENSES/*
* NOTICE.md
* in any/every file heading
  * SPDX-License-Identifier:

# Build Configuration

We use the `gnatkr` tool to get a (mostly) unique executable name on 16 (max) caracter length.
See the [gnatkr documentation](https://docs.adacore.com/gnat_ugn-docs/html/gnat_ugn/gnat_ugn/the_gnat_compilation_model.html#file-name-krunching-with-gnatkr) for the File Name Krunching algorithm.

```shell
$ cd gpr
$ gnatkr owner_pkg1_pkg2_mycomponent_tests.gpr 16
$ cd ..
```

```ada
   for Main use ("testsuite.adb");

   for Executable ("testsuite.adb") use "gaualgcrymd5test";  -- gnatkr *.adb 16
```

# Build

```shell
$ gprbuild
```

or

```shell
$ alr build
```

Results in:

```
build
└── development (or validation)(or release)
    └── x86_64-apple-darwin22.1.0
        ├── bin
        ├── lib
        └── obj
```

# Run

### On Unix-like

There could be a soft-link to the `build/*/*/bin` directory

```shell
$ ln -sv build/development/x86_64-apple-darwin22.1.0/bin bin
```

then,

```shell
$ bin/gaualgcrymd5test
```

### On Window$

?? up to you ...

```
build\development\x86_64\bin\gaualgcrymd5test.exe
```
